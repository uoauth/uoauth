=====================================================================
micro-OAuth: minimalist OAuth client for php
Copyright (C) 2011  Luis Sergio Moura

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
=====================================================================

The goal of this project is to provide a simple way to interact with OAuth clients, with no setup hassle.

This class is still on an early stage but it's working. You can use it with Twitter, given you know how OAuth works. It would be interesting to see classes or delegates that implements a high-level functionality for each OAuth service.
For now, here's some basic instruction on how to get somewhere.

-----

How to request token with uOAuth (example using values provided by official twitter documentation at https://dev.twitter.com/docs/auth/oauth)

<?php
	$oa = new uOAuth();
	$oa->oauth_callback = 'http://localhost:3005/the_dance/process_callback?service_provider_id=11';
	$oa->oauth_nonce = 'QP70eNmVz8jvdPevU3oJD2AfF7R7odC2XJcn4XlZJqk';
	$oa->consumerKey = 'GDdmIQH6jhtmLUypg82g';
	$oa->consumerSecret = 'MCD8BKwGdgPHvAuvgvz4EQpqDAtx89grbuNMRd7Eh98';
	$oa->oauth_timestamp = '1272323042';

	$x = $oa->request('POST', 'https://api.twitter.com/oauth/request_token');
	echo($x);
?>

Users MUST set consumerKey and consumerSecret values for the class to work. Otherwise, things will get nasty. One valid approach is to create a subclass that automatically sets values for you, like so:

<?php

require_once('uOAuth.php');

class myOAuth extends uOAuth {
	function __construct() {
		parent::__construct();
		
		# Your keys
		$this->consumerKey = 'secretKey';
		$this->consumerSecret = 'secretSecret';
	}
}

?>

Timestamp and Nonce are automatically generated if you do not provide them.