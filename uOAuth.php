<?php
# uOAuth
/*
	micro-OAuth: minimalist OAuth client for php
    Copyright (C) 2011  Luis Sergio Moura

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Similar to http_build_query, but we make our keys sorted, and we don't have many parameters....
function _uOA_sorted_http_build_query($params, $glue = '&') {
	$parameterKeys = array_keys($params);
	sort($parameterKeys);
	$parameterValues = array();
	foreach ($parameterKeys as $k) {
		$parameterValues[] = rawurlencode($k) . "=" . rawurlencode($params[$k]);
	}
	$parameterString = implode($glue, $parameterValues);
	
	return($parameterString);
}

class uOAuth {
	// Secret stuff. Fill these in. token and tokenSecret are optional (you should know when you need them)
	var $consumerSecret = '';
	var $consumerKey = '';
	var $token = '';
	var $tokenSecret = '';
	
	// Empty values are not sent to the server unless otherwise specified.
	var $oauth_signature_method = 'HMAC-SHA1'; // Only HMAC-SHA1 supported for now
	var $oauth_version = '1.0';
	var $oauth_timestamp = '';          // Empty = Autogenerate
	var $oauth_nonce = '';              // Empty = Autogenerate (default 43 characters long)
	var $oauth_callback = '';
	var $oauth_verifier = '';
	
	function uOAuth() {
		
	}
	
	function __construct() {
		
	}
	
	function generateNonce($size = 43) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$ret = "";
		for ($i = 0; $i < $size; $i++) {
			$idx = rand(0, strlen($chars) - 1);
			$ret .= $chars[$idx];
		}
		
		return($ret);
	}
	
	function _nonce() {
		if ($this->oauth_nonce != '')
			return($this->oauth_nonce);

		return($this->generateNonce());
	}
	
	function _timestamp() {
		if ($this->oauth_timestamp != '')
			return($this->oauth_timestamp);
			
		return(time());
	}
	
	// Generate our array of OAuth parameters with the available values
	function _authorizationParameters() {
		$authorizationParameters = array();
		
		$authorizationParameters['oauth_nonce'] = $this->_nonce();
		$authorizationParameters['oauth_timestamp'] = $this->_timestamp();
		
		if ($this->consumerKey != '') $authorizationParameters['oauth_consumer_key'] = $this->consumerKey;
		if ($this->token != '') $authorizationParameters['oauth_token'] = $this->token;

		if ($this->oauth_callback != '') $authorizationParameters['oauth_callback'] = $this->oauth_callback;
		if ($this->oauth_signature_method != '') $authorizationParameters['oauth_signature_method'] = $this->oauth_signature_method;
		if ($this->oauth_verifier != '') $authorizationParameters['oauth_verifier'] = $this->oauth_verifier;
		if ($this->oauth_version != '') $authorizationParameters['oauth_version'] = $this->oauth_version;
		
		return($authorizationParameters);
	}
	
	// params MUST be a string.
	function _rest($url, $params, $method, $headers) {
		// Borrowed from http://wezfurlong.org/blog/2006/nov/http-post-from-php-without-curl/
		$cparams = array(
			'http' => array(
				'method' => $method,
				'ignore_errors' => true
			)
		);
		if ($params != null) {
			if ($method == 'POST')
				$cparams['http']['content'] = $params;
			else
				$url .= '?' . $params;
		}
		if ($headers != null) {
			$cparams['http']['header'] = $headers;
		}
		
                //echo("<pre>" . print_r($cparams, true) . "</pre>\n");
		$context = stream_context_create($cparams);
		$fp = fopen($url, 'rb', false, $context);
		if (!$fp)
			return(false);

		$res = stream_get_contents($fp);
		
		return($res);
	}
	
	// The $parameters array MUST be in a key => value format
	// $url MUST be a clean URL. no "?" or "&" characters. Put those in the parameters.
	function request($method, $url, $parameters = array()) {
		// Grab authorization parameters
		$authorizationParameters = $this->_authorizationParameters();
		
		// Generate the request parameters with additional parameters.
		$requestParameters = $authorizationParameters;
		foreach ($parameters as $k => $v) {
			$requestParameters[$k] = $v;
		}
		
		$parameterString = _uOA_sorted_http_build_query($requestParameters);
		
		// Signature key
		$key = $this->consumerSecret . '&' . $this->tokenSecret;
		
		// Generate Signature base string -- RFC section 3.4.1
		$signatureBaseString = $method . '&' . rawurlencode($url) . '&' . rawurlencode($parameterString);
		$signature = base64_encode(hash_hmac('sha1', $signatureBaseString, $key, TRUE));
		
		// Put our signature on our authorizationParameters
		$authorizationParameters['oauth_signature'] = $signature;
				
		// Generate our OAuth Authorization HTTP Header
		$authHeaderValue = 'OAuth ' . _uOA_sorted_http_build_query($authorizationParameters, ', ');
		
		// Generate $params -- if needed
		if (empty($parameters)) {
			$params = null;
		}
		else {
			$params = _uOA_sorted_http_build_query($parameters);
		}
		
		$headers = "Authorization: $authHeaderValue\r\nContent-Type: application/x-www-form-urlencoded\r\n";
		if (false) {
			// Dump stuff!
                        echo("<pre>");
			echo("Key:        $key\n");
			echo("BaseString: $signatureBaseString\n");
			echo("Signature:  $signature\n");
			echo("Headers:    $headers\n");
                        echo("</pre>");
		}
		
		$ret = $this->_rest($url, $params, $method, $headers);
		
		return($ret);
	}
}
